import java.util.Scanner;

public class CounterRunnable implements Runnable{
    private int count = 0;
    public synchronized void run() {
        Scanner scn = new Scanner(System.in);
        while(!Thread.interrupted()){
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
            count+=1;
            System.out.println("The count now is: "+count);
        }
    }
    public int getCounter(){
        return this.count;
    }

    public static void main(String[] args) throws InterruptedException {
        CounterRunnable counterRunnable = new CounterRunnable();
        Thread tread1 = new Thread(counterRunnable);
        tread1.start();
//        System.out.println(counterRunnable.getCounter());
    }
}
