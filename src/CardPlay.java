import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

public class CardPlay implements Comparable{


    private String suits;
    private String values;

    public Map<String,String> cards = new HashMap<>();
    public Set<CardPlay> cardsSet = new TreeSet<>();

    public Set<CardPlay> getCardsSet() {
        return cardsSet;
    }

    public void setCardsSet(Set<CardPlay> cardsSet) {
        this.cardsSet = cardsSet;
    }


    public CardPlay(String suits, String values){
        this.suits = suits;
        this.values = values;
        this.cards.put(suits,values);
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj){
            return true;
        }
        if (this ==null){
            return false;
        }
        if (obj instanceof CardPlay){
            CardPlay actualInstance = (CardPlay) obj;
            if ((actualInstance.values == this.values)){
                return true;
            }
        }
        return super.equals(obj);
    }

    public static void main(String[] args) {
        CardPlay trials = new CardPlay("H","3");
        CardPlay newTrial = new CardPlay("S","3");
        System.out.println(trials.equals(newTrial));
        System.out.println(trials.compareTo(newTrial));

    }

    @Override
    public int compareTo(Object o) {
        CardPlay actualInstance = (CardPlay) o;
        if(this.values.compareTo(actualInstance.values)==0){
            return this.suits.compareTo(actualInstance.suits);
        }
        else{
            return this.values.compareTo(actualInstance.values);
        }
    }
}
