public class CounterThread extends Thread{
    private int count = 0;
    @Override
    public void run()
    {
        while(!Thread.interrupted()){
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
            count+=1;
        System.out.println("The count now is: "+count);
    }
    }

    public static void main(String[] args) throws InterruptedException {
        Thread thread1 = new CounterThread();
        thread1.start();
        System.out.println(thread1.getPriority());
    }
}
