public enum Suits {
    S(0), D(1),C(2),H(3);
    private int suitNumber;

    public int getSuitNumber() {
        return suitNumber;
    }

    Suits(int number) {
        this.suitNumber = number;
    }
}
