public class randClass {
    public static void main(String[] args) {
        //Using do-while loop to check the range.
//        int number;
//        do {
//            number = (int) (Math.random() * 6) - 3;
//            System.out.println(number);
//        } while (number != 0);
        //Using a while loop
        int number = 1;
        while(number !=0){
            number = (int) (Math.random()*6)-3;
            System.out.println(number);
        }
    }
}
