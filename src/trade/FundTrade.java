package trade;

import exceptions.TradeIDException;

public class FundTrade extends Trade {
    private final double dividend;



    //Constructor
    public FundTrade(String id, String Symbol, int quantity,double price) throws TradeIDException {
        super(id,Symbol,quantity,price);
        this.dividend = (price)*0.3;
    }
    public double calcDividend(){
        return this.dividend;
    }
}