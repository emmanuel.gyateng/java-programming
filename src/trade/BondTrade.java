package trade;

public class BondTrade extends Trade {
    private final double dividend = 30.00;
    public BondTrade(String id, String Symbol, int quantity){
        super( id,  Symbol, quantity);
    }

    public double calcDividend(){
        return this.dividend;
    }


}
