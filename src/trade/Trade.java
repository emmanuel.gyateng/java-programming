package trade;

import exceptions.TradeIDException;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

public abstract class Trade {
    private String id;
    private  String Symbol;
    private int quantity;
    private double price;
    private static List <String> idArrays = new ArrayList<>();
    private final LocalDate currentDate = LocalDate.now();
    private final  LocalTime currentTime = LocalTime.now();

    public Trade(String id, String Symbol, int quantity, double price) throws TradeIDException {
        if(idArrays.contains(id)){
            throw new TradeIDException("ID can not be duplicated");
        }
        else{
            idArrays.add(id);
        }
        this.id = id;
        this.Symbol = Symbol;
        this.quantity = quantity;
        this.price = price;
    }
    public  Trade(String id, String Symbol, int quantity){
        this.id = id;
        this.Symbol = Symbol;
        this.quantity = quantity;
    }

    public String toString(){
        return "id: "+ this.id+ " Symbol: "+ this.Symbol+" quantity: "+ this.quantity+ " price: "+ this.price;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price){
        if(price >0){
        this.price = price;
        }
        else{
            System.out.println("Can not create negative prices");
        }
    }

    public abstract double calcDividend();
}
