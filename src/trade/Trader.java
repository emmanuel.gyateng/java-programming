package trade;

public class Trader {
    private String name;
    private Account account = new Account();

    public Trader(String name) {
        this.name = name;
    }

    public Account getAccount() {
        return account;
    }


    public void addTrade(Trade trade) {
        this.account.setTotalTrades(trade.getQuantity() * trade.getPrice());
    }
}