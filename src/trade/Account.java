package trade;

import java.util.ArrayList;
import java.util.List;

public class Account {
     private double totalTrades;
     private List<Trade> trade = new ArrayList<>();

     public List<Trade> getTrade() {
          return this.trade;
     }

     public void setTrade(Trade trade) {
          this.trade.add(trade);
     }

     public void setTotalTrades(double totalTrades){
          this.totalTrades = totalTrades;
     }
     public double getTotalTrades() {
          return this.totalTrades;
     }
     public void printTrades(){
          for(Trade i: trade){
               System.out.println(i);
          }
     }
}
