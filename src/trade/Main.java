package trade;

import clients.Client;
import exceptions.TradeIDException;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
//        Client client = new Client("Emmanuel","Gyateng");
//        System.out.println(client.getMembershipMaxTrades());
        Account acc = new Account();
        try{
            Trade trade1 = new FundTrade("Add", "APPL", 100, 15.25);
            acc.setTrade(trade1);
            Trade trade2 = new FundTrade("Add", "APPL", 100, 9.0);
            acc.setTrade(trade2);
        }catch (TradeIDException e){
            System.out.println(e.getMessage());
        }finally {
            acc.printTrades();
        }




//        }
//        Trader trader = new Trader("Emmanuel Okyere");
//        System.out.println(trader.getAccount());
//        LocalDate currentDate = LocalDate.now();
//        LocalTime currentTime = LocalTime.now();
//        System.out.println(currentDate);
//        System.out.println(currentTime);
//        trade.Trade trade = new trade.Trade("T1", "APPL", 100, 15.25);
//        trader.addTrade(trade);
//        System.out.println(trade);

//        List<String> myArray = new ArrayList<>() {
//        };
//        myArray.add("B");
//        myArray.add("A");
//        myArray.add("C");
//
//        for(String i: myArray){
//            System.out.print(i+"\n");
//        }
//        Collections.sort(myArray);
//        for(String i: myArray){
//            System.out.print(i+"\n");
//        }
    }
}
