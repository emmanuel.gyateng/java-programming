package clients;

import trade.Trade;

import java.util.Objects;

public class Client {
    private String firstName, secondName;
    private MemberShipType membershipType;
    private int totalTrades;
    public int getTotalTrades() {
        return totalTrades;
    }

    public void setTotalTrades() {
        this.totalTrades++;
    }



    public String getFirstName() {
        return firstName;
    }

    public Client(String firstName, String secondName) {
        this.firstName = firstName;
        this.secondName = secondName;
        this.membershipType = MemberShipType.BRONZE;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSecondName() {
        return secondName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public MemberShipType getMenbershiptype() {
        return membershipType;
    }

    public void setMenbershiptype(MemberShipType membershipType) {
        this.membershipType = membershipType;
    }
    public int getMembershipMaxTrades(){
        return this.membershipType.maxNumberOfTrades();
    }

    public void addTrade(Trade trade){


    }
    public boolean canTrade(){
        switch (this.membershipType) {
            case BRONZE -> {
                if (this.totalTrades < MemberShipType.BRONZE.maxNumberOfTrades()) {
                    System.out.println("Can Trade now");
                    return true;
                }
                System.out.println("Can not trade now, Please try later");
            }
            case SILVER -> {
                if (this.totalTrades < MemberShipType.SILVER.maxNumberOfTrades()) {
                    System.out.println("Can trade");
                    return true;
                }
                System.out.println("Exceeded max trade for a day");
            }
            case GOLD -> {
                System.out.println("Can trade till till till");
                return true;
            }
        }
        return true;
    }
}
