package clients;
import java.time.LocalTime;
public enum MemberShipType{
    BRONZE{
        public int maxTrade(){
            if (LocalTime.now().getHour() > 10) {
                return 10;
            }
            else{
                return 0;
            }
        }
    }, SILVER(10), GOLD(20);
    private int maxTrades;

    MemberShipType(int maxTrades){
        this.maxTrades =maxTrades;
    }

    MemberShipType() {

    }

    public int maxNumberOfTrades(){
        return maxTrades;
    }
}
