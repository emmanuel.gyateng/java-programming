public class CreateArray {

    private int[] array;
    public CreateArray(int size){
        this.array = new int[size];
        for (int i = 0; i<size; ++i){
            this.array[i]=i+1;
        }

    }

    public int[] getArray() {
        return this.array;
    }

}
