import java.util.ArrayList;
import java.util.List;

public class StackGeneric <T>{

    private List<T> stack = new ArrayList<T>();

    public void push(T t){
        this.stack.add(t);
    }
    public T top(){
        return this.stack.get(stack.size()-1);
    }

    public  void pop(){

        this.stack.remove(stack.size()-1);

    }
    public int size(){
        return this.stack.size();
    }



    public static void main(String[] args) {
        StackGeneric <Integer> news = new StackGeneric<>();
        news.push(3);
        news.push(4);
        news.push(5);
        System.out.println(news.top());
        news.pop();
        System.out.println(news.top());
        System.out.println(news.size());


    }
}
