import exceptions.TradeIDException;
import org.junit.Test;
import trade.Account;
import trade.FundTrade;
import trade.Trade;
import static org.junit.Assert.*;
public class AccountAddTradesTest {

    @Test
    public void accountAddTradesArrayTest() throws TradeIDException {

        Account account = new Account();
        Trade trade1 = new FundTrade("T1", "APPL", 100, 15.25);
        account.setTrade(trade1);
        assertThrows(TradeIDException.class,()->{Trade trade2 = new FundTrade("T1", "APPL", 100, 9.0);});
    }
}
