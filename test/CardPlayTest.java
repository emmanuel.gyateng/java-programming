import org.junit.Test;
import static org.junit.Assert.*;

public class CardPlayTest {
    @Test
    public void equalsTest(){
        CardPlay trials = new CardPlay("S","3");
        CardPlay newTrial = new CardPlay("S","3");
        assertTrue(trials.equals(newTrial));
    }
}
