import exceptions.TradeIDException;
import org.junit.Test;
import trade.FundTrade;

import static org.junit.Assert.*;
public class FundTradeTest {

    @Test
    public void fundTradeTest() throws TradeIDException {
        FundTrade fundtrade = new FundTrade("TD","TF",12,3.3);
        assertEquals(100, fundtrade.calcDividend(),0.0);
    }
}
