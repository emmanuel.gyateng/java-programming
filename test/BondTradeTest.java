import org.junit.Test;
import trade.BondTrade;

import static org.junit.Assert.*;
public class BondTradeTest {
    @Test
    public void testBondTrade(){

        BondTrade bondtrade = new BondTrade("TD","TF",30);
        assertEquals(30, bondtrade.calcDividend(), 0.0);
    }
}
