import org.junit.Test;

import static org.junit.Assert.*;
public class StackGenericTest {
    StackGeneric <Integer> news = new StackGeneric<>();
    @Test
    public void pushTest(){
        news.push(3);
        news.push(4);
        news.push(5);
        assertEquals(3,news.size());

    }
    @Test
    public void popTest(){
        news.push(3);
        news.push(4);
        news.push(5);
        news.pop();
        assertEquals(2,news.size());

    }
    @Test
    public void topTest(){
        news.push(3);
        news.push(4);
        news.push(5);
        assertEquals(5,news.top(),0.00);

    }
    @Test
    public void sizeTest(){
        news.push(3);
        news.push(4);
        news.push(5);
        assertEquals(3, news.size());
    }
}
